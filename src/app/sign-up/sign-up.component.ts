import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {
  email:string;
  password:string;
  confirmPassword:string;
  errorMessage:string;
  isError:boolean = false;
  confirmPasswordError:boolean = false;

  onSubmit(){
    this.confirmPasswordError=false;
    if (this.confirmPassword !== this.password){
      this.confirmPasswordError=true;
    }
    else{
      this.auth.register(this.email,this.password).then(
        res => {
        console.log('Succesful login;');
        this.router.navigate(['/mydiamonds']);
      }).catch(
        err => {
          console.log(err);
          this.isError = true;
          this.errorMessage = err.message;
        }
      )
    }
  }

  back(){
    this.router.navigate(['/welcome']);
  }

 



  constructor(private auth:AuthService, private router:Router) { }

  ngOnInit(): void {
  }

}
