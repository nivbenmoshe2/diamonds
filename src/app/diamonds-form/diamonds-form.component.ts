import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';
import { DiamondsService } from '../diamonds.service';
import { ImageService } from '../image.service';
import { PredictionService } from '../prediction.service';

@Component({
  selector: 'app-diamonds-form',
  templateUrl: './diamonds-form.component.html',
  styleUrls: ['./diamonds-form.component.css']
})
export class DiamondsFormComponent implements OnInit {
  isLinear = true;
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;

  userId:string;
  timestamp:number =Date.now();
  weight:number;
  color:string;
  cut:string;
  shape:string;
  clarity:string;
  x:number;
  y:number;
  z:number;
  table:number;
  depth:number;
  price:number;
  image:string;
  details:string;



  saveDiamond(){
    this.diamondsService.addDiamond(this.userId, this.weight, this.color, this.clarity, this.shape, this.price, this.image, this.details, this.timestamp)
    this.router.navigate(['/mydiamonds']);
}

 cancel(){
  this.weight = null;
  this.color = null;
  this.clarity = null;
  this.cut = null;
  this.shape = null;
  this.x = null;
  this.y = null;
  this.z = null;
  this.table = null;
  this.depth = null;
  this.price = null;
  this.image = null;
  this.details = null;
}


predict(){
  this.predictionService.predict(this.weight, this.cut, this.color,this.clarity, this.depth, this.table, this.x, this.y, this.z).subscribe(
    res => {
      console.log(res);
      this.price = Math.round(res);
      console.log(this.price);
    }
  );
}

getImage(){
  console.log(this.shape);
  this.image = this.imageService.images[this.shape];
  console.log(this.image);
}
  

  constructor(private predictionService:PredictionService, private diamondsService:DiamondsService, public authService:AuthService , private router:Router, private _formBuilder: FormBuilder, private imageService:ImageService) { }

  ngOnInit(): void {
    this.firstFormGroup = this._formBuilder.group({
      weight: ['', Validators.min(0)],
      color: ['', Validators.required],
      clarity: ['', Validators.required],
      cut: ['', Validators.required],
      shape: ['', Validators.required],
      x: ['', Validators.min(0)],
      y: ['', Validators.min(0)],
      z: ['', Validators.min(0)],
      table: ['', [Validators.min(0.01), Validators.max(88)]],
      depth: ['', [Validators.min(0.01), Validators.max(93.1)]],
      price: ['', Validators.min(0)],
    });
    this.secondFormGroup = this._formBuilder.group({
      image: ['', Validators.required],
      details: ['', Validators.required],
    });
    this.authService.user.subscribe(
      user => {
        this.userId = user.uid;
       }
    )
    
  }

}
