import { Diamond } from './interfaces/diamond';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class DiamondsService {
  private URL = "https://api.midonline.com/api/QueryApi/GetInventory?q=qqR9BP3NvbauADjWoT5cgQ%3d%3d" ;

  userCollection:AngularFirestoreCollection = this.db.collection('users');
  mydiamondsCollection:AngularFirestoreCollection;
  

  getDiamonds():Observable<Diamond>{
    return this.http.get<Diamond>(`${this.URL}`);
  }

  addDiamond(userId:string, weight:number, color:string, clarity:string, shape:string, price:number, image:string, details:string, timestamp:number){
    const diamond = {weight:weight ,color:color, clarity:clarity ,shape:shape, price:price, image:image, details:details, timestamp:timestamp, sold:0};
    this.userCollection.doc(userId).collection('diamonds').add(diamond);
    
  }

  getCollectionDiamonds(userId){
    this.mydiamondsCollection = this.db.collection(`users/${userId}/diamonds`,
    ref => ref.orderBy('timestamp','desc'));
    return this.mydiamondsCollection.snapshotChanges();
  }



  updateSold(userId:string, id:string,sold:number){
    this.db.doc(`users/${userId}/diamonds/${id}`).update(
       {
        sold:sold
       }
     )
  }

  constructor(private http: HttpClient ,private db: AngularFirestore) { }
}
