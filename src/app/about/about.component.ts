import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit {

  webs = [{title:'GIA', subtitle:'GIA is an independent nonprofit that protects the gem and jewelry buying.', image:'http://www.gia.edu/img/GIA_Logo.png', summary:'GIA is the world’s foremost authority on diamonds, colored stones, and pearls. A public benefit, nonprofit institute, GIA is the leading source of knowledge, standards, and education in jewelry.', link:'https://4cs.gia.edu/en-us/'},
          {title:'Kaggle', subtitle:'Kaggle is the world is largest data science community.', image:'https://cdn.freelogovectors.net/wp-content/uploads/2018/06/kaggle-logo.png', summary:'This classic dataset contains the prices and other attributes of almost 54,000 diamonds. It is a great dataset for beginners learning to work with data analysis and visualization.', link:'https://www.kaggle.com/shivam2503/diamonds'},
          {title:'MID', subtitle:'MID House of Diamonds is manufacturer & distributor of wholesale diamonds.', image:'https://media.messe.ch/baselworld/OnlineCatalogue/images/logo/600x/595180.png', summary:' MID House of Diamonds allowed to create an easy-to-use online API module, including links to images of the diamonds and their respective grading reports.', link:'https://www.middiamonds.com/'},
          {title:'AWS', subtitle:'Amazon Web Services offers reliable, scalable, and inexpensive cloud computing services.', image:'https://logos-download.com/wp-content/uploads/2016/12/Amazon_Web_Services_logo_AWS.png', summary:'Amazon Web Services (AWS) is the world’s most comprehensive and broadly adopted cloud platform, offering over 200 fully featured services from data centers globally.', link:'https://aws.amazon.com/'}
        ];


  constructor() { }

  ngOnInit(): void {
  }

}
