import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PredictionService {

  private url = "https://62hupce4v5.execute-api.us-east-1.amazonaws.com/beta";

  predict(carat:number, cut:string, color:string, clarity:string, depth:number, table:number, x:number, y:number, z:number){
    let json = { 
        'carat': carat,
        'cut': cut,
        'color': color,
        'clarity': clarity,
        'depth': depth,
        'table': table,
        'x': x,
        'y': y,
        'z': z
      }
    
    let body = JSON.stringify(json);
    return this.http.post<any>(this.url, body).pipe(
      map(res => {
        console.log(res);
        let final = res.body;
        console.log(final);
        final = final.replace('[','');
        final = final.replace(']','');
        console.log(final);
        return final;
      })
    )
  }


  constructor(private http:HttpClient) { }
}
