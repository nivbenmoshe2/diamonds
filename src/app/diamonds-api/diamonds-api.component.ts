import { Diamond } from './../interfaces/diamond';
import { DiamondsService } from './../diamonds.service';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'diamonds-api',
  templateUrl: './diamonds-api.component.html',
  styleUrls: ['./diamonds-api.component.css']
})
export class DiamondsApiComponent implements OnInit {
  diamonds$:Observable<Diamond>;
  userId:string;
  timestamp:number =Date.now();

  gridColumns = 4;

  toggleGridColumns() {
    this.gridColumns = this.gridColumns === 3 ? 4 : 3;
  }

  saveDiamond(weight:number, color:string, clarity:string, shape:string, price:number, image:string, details:string){
    this.diamondsService.addDiamond(this.userId, weight, color, clarity, shape, price, image, details, this.timestamp)
    this.router.navigate(['/mydiamonds']);
 }

  constructor(private diamondsService:DiamondsService, public authService:AuthService , private router:Router) { }

  ngOnInit(): void {
    this.diamonds$ = this.diamondsService.getDiamonds();
    this.authService.user.subscribe(
      user => {
        this.userId = user.uid;
       }
    ) 
  }

}
