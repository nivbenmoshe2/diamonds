import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'parameters',
  templateUrl: './parameters.component.html',
  styleUrls: ['./parameters.component.css']
})
export class ParametersComponent implements OnInit {

  parameters = [
                {title:'COLOR', subtitle:'Diamond Color Actually Means Lack of Color', summary:'Understanding what diamond color means helps in choosing the right diamond. Interestingly, the diamond color evaluation of most gem-quality diamonds is based on the absence of color. A chemically pure and structurally perfect diamond has no hue, like a drop of pure water, and consequently, a higher value. GIA’s D-to-Z diamond color-grading system measures the degree of colorlessness by comparing a stone under controlled lighting and precise viewing conditions to masterstones of established color value.', image:'https://4cs.gia.edu/wp-content/uploads/2017/05/Hero_Color_700x394.jpg', link:'https://4cs.gia.edu/en-us/diamond-color/'},
                {title:'CLARITY', subtitle:'Diamond Clarity Refers to the Absence of Inclusions and Blemishes', summary:'To understand diamond clarity, we must first understand how diamonds are created. Natural diamonds are the result of carbon exposed to tremendous heat and pressure deep in the earth. This process can result in a variety of internal characteristics called ‘inclusions’ and external characteristics called ‘blemishes.’', image:'https://4cs.gia.edu/wp-content/uploads/2017/05/Hero_Clarity_700x394-01.jpg', link:'https://4cs.gia.edu/en-us/diamond-clarity/'},
                {title:'CUT', subtitle:'Understanding Diamond Cut', summary:'Diamonds are renowned for their ability to transmit light and sparkle so intensely. We often think of a diamond’s cut as shape (round, heart, oval, marquise, pear), but what diamond cut actually does mean how well a diamond’s facets interact with light. Precise artistry and workmanship are required to fashion a stone so its proportions, symmetry and polish deliver the magnificent return of light only possible in a diamond.', image:'https://4cs.gia.edu/wp-content/uploads/2017/05/Hero_Cut_700x394.jpg', link:'https://4cs.gia.edu/en-us/diamond-cut/'},
                {title:'CARAT WEIGHT', subtitle:'Diamond Carat Weight Measures a Diamond’s Apparent Size', summary:'To put it simply, diamond carat weight measures how much a diamond weighs.', image:'https://4cs.gia.edu/wp-content/uploads/2017/05/Hero-Carat-Weight_700x394.jpg', link:'https://4cs.gia.edu/en-us/diamond-carat-weight/'}];


  constructor() { }

  ngOnInit(): void {
  }

}
