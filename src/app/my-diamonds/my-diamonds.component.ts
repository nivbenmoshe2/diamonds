import { DiamondsService } from './../diamonds.service';
import { Diamond } from './../interfaces/diamond';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthService } from '../auth.service';
import { SalesService } from '../sales.service';
import { AwsService } from '../aws.service';

@Component({
  selector: 'app-my-diamonds',
  templateUrl: './my-diamonds.component.html',
  styleUrls: ['./my-diamonds.component.css']
})
export class MyDiamondsComponent implements OnInit {

  diamonds$:Observable<any>;
  userId:string;
  gridColumns = 4;
  diamonds:Diamond[];
  diamondSold:Diamond[];
  CardToSale = []; 
  date: string;
  hour: string;
  customer: string;
  email:string;
  timestamp:number =Date.now();


  toggleGridColumns() {
    this.gridColumns = this.gridColumns === 3 ? 4 : 3;
  }

  

  saleDiamond(id:string, sold:number, weight:number, color:string, clarity:string, shape:string, price:number, image:string, details:string){
    this.salesServie.addSale(this.email, this.customer, this.date, this.hour, weight, color, clarity, shape, price, image, details, this.timestamp);
    this.AwsService.sns(this.customer, this.date, price).subscribe(res => {
      console.log(res);
    });
    sold = 1 ; 
    this.diamondsServie.updateSold(this.userId,id,sold);
    this.customer = null;
    this.date = null;
    this.hour = null;
  }

  cancel(){
    this.customer = null;
    this.date = null;
    this.hour = null;
  }
  

  constructor(private diamondsServie:DiamondsService, public authService:AuthService, private salesServie:SalesService, private AwsService:AwsService) { }

  ngOnInit(): void {

    this.authService.user.subscribe(
      user => {
        this.userId = user.uid;
        this.email = user.email;
        console.log(this.userId);
        this.diamonds$ = this.diamondsServie.getCollectionDiamonds(this.userId);
        this.diamonds$.subscribe(
          docs => {         
            this.diamonds = [];
            this.diamondSold = [];
            var i = 0;
            for (let document of docs) {
              const diamond:Diamond = document.payload.doc.data();
              if(diamond.sold == 0 ){
                diamond.id = document.payload.doc.id;
                this.diamonds.push(diamond); 
              }
              else{
              diamond.id = document.payload.doc.id;
                 this.diamondSold.push(diamond); 
              }
            }  
          }
        )
    })
       }
    
  

}
