import { AwsService } from './../aws.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-reko-aws',
  templateUrl: './reko-aws.component.html',
  styleUrls: ['./reko-aws.component.css']
})
export class RekoAwsComponent implements OnInit {
  image:string ="niv.jpeg";
  name:string;
  city:string;
  photo:string ="https://nivbn-my-upload-bucket-01.s3.amazonaws.com/"+"anonymous.png";
  path:string ="https://nivbn-my-upload-bucket-01.s3.amazonaws.com/";
  match:boolean;
  nomatch:boolean;
  uploudedphoto: boolean;
  fileObj: File;
  imagename:string;
  birthDate:string;
  phone:string;


  reko(){
    this.AwsService.reko(this.imagename).subscribe(
      res => {
        if (res.manager_name =="no match"){
          this.name = res.manager_name;
          this.photo = this.path+"Faildpic.jpg";
          this.match = false;
          this.nomatch = true;
        }else{
        this.name = res.manager_name;
        this.city = res.city;
        this.phone = res.phone;
        this.birthDate = res.birthDate;
        this.photo = this.path+res.photo;
        this.nomatch = false;
        this.match = true;
        }
        console.log(res);
      }
    );
  }

  
  
  onFileUpload(event: Event): void{
    console.log(event);
    const FILE = (event.target as HTMLInputElement).files[0];
    this.fileObj = FILE;
    console.log(this.fileObj);
    
    const fileForm = new FormData();
    fileForm.append('img', this.fileObj);
    this.AwsService.fileUpload(fileForm).subscribe(res => {
      console.log(res);
      this.imagename = res.imgName;
      this.photo = this.path+this.imagename;
      console.log(this.imagename);
      this.uploudedphoto = true;
      this.match = false;
      this.nomatch = false;
    });
  }

  constructor(private AwsService:AwsService) { }

  ngOnInit(): void {
  }

}
