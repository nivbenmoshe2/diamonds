import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AboutComponent } from './about/about.component';
import { DiamondsApiComponent } from './diamonds-api/diamonds-api.component';
import { DiamondsFormComponent } from './diamonds-form/diamonds-form.component';
import { LoginComponent } from './login/login.component';
import { MyDiamondsComponent } from './my-diamonds/my-diamonds.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { ParametersComponent } from './parameters/parameters.component';
import { RekoAwsComponent } from './reko-aws/reko-aws.component';
import { SalesComponent } from './sales/sales.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { WelcomeComponent } from './welcome/welcome.component';

const routes: Routes = [
  { path: 'welcome', component: WelcomeComponent },
  { path: 'login', component: LoginComponent },
  { path: 'signup', component: SignUpComponent },
  { path: 'diamonds', component: DiamondsApiComponent },
  { path: 'mydiamonds', component: MyDiamondsComponent },
  { path: 'parameters', component: ParametersComponent },
  { path: 'about', component: AboutComponent },
  { path: 'diamondsform', component: DiamondsFormComponent },
  { path: 'sales', component: SalesComponent },
  { path: 'compface', component: RekoAwsComponent },
  { path: '', redirectTo: '/welcome' ,pathMatch: 'full'},
  { path: '**', component: NotFoundComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
