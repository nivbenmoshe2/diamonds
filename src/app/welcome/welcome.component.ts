import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {

  email:string;

  webs = [{title:'GIA', subtitle:'GIA is an independent nonprofit that protects the gem and jewelry buying.', image:'http://www.gia.edu/img/GIA_Logo.png', summary:'Established in 1931, GIA is the world’s foremost authority on diamonds, colored stones, and pearls. A public benefit, nonprofit institute, GIA is the leading source of knowledge, standards, and education in gems and jewelry.', link:'https://4cs.gia.edu/en-us/'},
           {title:'Kaggle', subtitle:'Kaggle is the world is largest data science community with powerful tools and resources to help you achieve your data science goals.', image:'https://cdn.freelogovectors.net/wp-content/uploads/2018/06/kaggle-logo.png', summary:'This classic dataset contains the prices and other attributes of almost 54,000 diamonds. It is a great dataset for beginners learning to work with data analysis and visualization.', link:'https://www.kaggle.com/shivam2503/diamonds'},
           {title:'MID', subtitle:'MID House of Diamonds is the world is premier manufacturer & distributor of wholesale diamonds & loose diamonds', image:'https://dearkingteacher.files.wordpress.com/2016/10/mid-diamonds.png?w=1180&h=435&crop=1', summary:'created an easy-to-use online API module, via which the ecommerce retailer is able to set criteria as to what specific stones should be included from MID’s inventory, including links to images of the diamonds and their respective grading reports.', link:'https://www.middiamonds.com/'}];

  constructor(public authService:AuthService) { }

  ngOnInit(): void {
    this.authService.user.subscribe(
      user => {
        this.email = user.email;
      }
    )
  }

}
