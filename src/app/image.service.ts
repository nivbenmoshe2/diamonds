import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ImageService {

  public images = {Round:'https://firebasestorage.googleapis.com/v0/b/diamonds-c8d84.appspot.com/o/round.jpg?alt=media&token=36f29b3c-85e1-45ef-baa5-491894d4c315',
                  Princess:'https://firebasestorage.googleapis.com/v0/b/diamonds-c8d84.appspot.com/o/princess.jpg?alt=media&token=fc693f15-20c5-431a-8b26-a1c66a2ff592',
                  Emerald:'https://firebasestorage.googleapis.com/v0/b/diamonds-c8d84.appspot.com/o/emerald.jpg?alt=media&token=9d4d0308-0a89-493a-9ac7-19896bd4ae4f',
                  Asscher:'https://firebasestorage.googleapis.com/v0/b/diamonds-c8d84.appspot.com/o/asscher.jpg?alt=media&token=448725c6-b6bc-4972-b62a-b9a7cf4b05d7',
                  Oval:'https://firebasestorage.googleapis.com/v0/b/diamonds-c8d84.appspot.com/o/oval.jpg?alt=media&token=2051d859-1037-4111-8328-9f44f5436260',
                  Pear:'https://firebasestorage.googleapis.com/v0/b/diamonds-c8d84.appspot.com/o/pear.jpg?alt=media&token=79f8bc11-3921-441c-a96b-e22fc7d34dc7',
                  Marquise:'https://firebasestorage.googleapis.com/v0/b/diamonds-c8d84.appspot.com/o/marquise.jpg?alt=media&token=4097a9cb-aab0-4841-b7bb-d9289282b479'};

  constructor() {
    this.images;
   }
}
