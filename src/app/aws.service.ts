import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AwsService {

  private URL = "http://ec2-34-205-74-236.compute-1.amazonaws.com";
  private rekognation = "compare_face";
  private upload = "upload_image";
  private email = "send_email";

  constructor(private http: HttpClient) { }

  fileUpload(img: FormData):Observable<any> {
    return this.http.post(`${this.URL}/${this.upload}`,img);
    }

  reko(photoName:string):Observable<any> {
    return this.http.get(`${this.URL}/${this.rekognation}/${photoName}`);
    }

  sns(customer:string, date:string, price:number):Observable<any> {
    return this.http.get(`${this.URL}/${this.email}?customer=${customer}&price=${price}&date=${date}`);
    }
}
