import { Time } from '@angular/common';
import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SalesService {

  salesCollection:AngularFirestoreCollection = this.db.collection('sales');

  addSale(email:string,customer:string, date:string, hour:string, weight:number, color:string, clarity:string, shape:string, price:number, image:string, details:string, timestamp:number){
    const sale = {email:email,customer:customer, date:date, hour:hour, weight:weight ,color:color, clarity:clarity ,shape:shape, price:price, image:image, details:details, timestamp:timestamp};
    this.salesCollection.add(sale);
  }

  getSales():Observable<any[]>{
    this.salesCollection = this.db.collection(`sales`);
    return this.salesCollection.snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data();
        data.id = a.payload.doc.id;
        return { ...data };
      }))
    ); 
  }

  constructor(private db: AngularFirestore) { }
}
