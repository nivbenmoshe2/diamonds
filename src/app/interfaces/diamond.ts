export interface Diamond {
    id:string,
    weight:number,
    color:string,
    clarity:string,
    shape:number,
    price:number,
    image:string,
    details:string,
    timestamp?:number;
    sold?:number;
}
